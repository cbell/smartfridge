package utilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.plaf.basic.BasicScrollBarUI;

@SuppressWarnings("serial")
public class MyScrollingList  extends JPanel {
    private JScrollPane listScroller = null;
    private JList<Object> contentList = null;
    private DefaultListModel<Object> listModel;
    private ActionListener al;
    private boolean removable = false;

    public MyScrollingList(Object[] currentContent) {
        setLayout(new GridLayout());
        setBackground(Settings.instance().background);
        
        listModel = new DefaultListModel<Object>();
        for (Object object : currentContent) {
            listModel.addElement(object);
        }
        
        contentList = new JList<Object>(listModel);
        contentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        contentList.setLayoutOrientation(JList.VERTICAL);
        contentList.setVisibleRowCount(-1);
        contentList.setFocusable(true);
        contentList.setCellRenderer(new MyCellRenderer());
        contentList.setBackground(Settings.instance().background);
        ComponentDragScrollListener l = new ComponentDragScrollListener(contentList);
        contentList.addMouseMotionListener(l);
        contentList.addMouseListener(l);
        contentList.addHierarchyListener(l);
        contentList.addMouseMotionListener(new MyListMouseListener());
        contentList.addMouseListener(new MyListMouseListener());
        
        listScroller = new JScrollPane(contentList);
        listScroller.setPreferredSize(new Dimension(250, 80));
        listScroller.setBackground(Settings.instance().background);
        listScroller.getVerticalScrollBar().setPreferredSize (new Dimension(8,0));
        listScroller.getVerticalScrollBar().setEnabled(false);
        JScrollBar vBar = listScroller.getVerticalScrollBar();
        vBar.setUI(new BasicScrollBarUI()
        {   
            protected JButton createDecreaseButton(int orientation) {
                return createZeroButton();
            }
  
            protected JButton createIncreaseButton(int orientation) {
                return createZeroButton();
            }

            private JButton createZeroButton() {
                JButton jbutton = new JButton();
                jbutton.setPreferredSize(new Dimension(0, 0));
                jbutton.setMinimumSize(new Dimension(0, 0));
                jbutton.setMaximumSize(new Dimension(0, 0));
                return jbutton;
            }
            
            @Override
            protected void paintThumb(final Graphics g, final JComponent c, final Rectangle thumbBounds) {
                if (thumbBounds.isEmpty() || !this.scrollbar.isEnabled()) {
                    return;
                }
                g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.5f));
                g.fillRoundRect(thumbBounds.x, thumbBounds.y+5, thumbBounds.width, thumbBounds.height - 10, thumbBounds.width, thumbBounds.width);
            }

            @Override
            protected void paintTrack(final Graphics g, final JComponent c, final Rectangle trackBounds) {
                g.setColor(Settings.instance().background);
                g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
                if (this.trackHighlight == BasicScrollBarUI.DECREASE_HIGHLIGHT) {
                    this.paintDecreaseHighlight(g);
                } else if (this.trackHighlight == BasicScrollBarUI.INCREASE_HIGHLIGHT) {
                    this.paintIncreaseHighlight(g);
                }
            }
        });
        vBar.setBorder(BorderFactory.createEmptyBorder());
        vBar.setBackground(Settings.instance().background);
        vBar.setForeground(new Color(0.0f, 0.0f, 0.0f, 0.5f));
        listScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        listScroller.setAutoscrolls(true);

        add(listScroller);
    }
    
    public boolean isRemovable() {
        return removable;
    }

    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    public void setActionListener(ActionListener a) {
        al = a;
    }
    
    public void addItems(Object[] items) {
        for (Object object : items) {
            listModel.addElement(object);
        }
    }
    
    public void removeItems(Object[] items) {
        for (Object object : items) {
            listModel.removeElement(object);
        }
    }
    
    public void removeSelected() {
        if (contentList.getSelectedValue() != null) {
            listModel.removeElement(contentList.getSelectedValue());
        }
    }
    
    public void removeAll() {
        listModel.removeAllElements();
    }
    
    public int getLength() {
        return listModel.getSize();
    }
    
    private class MyListMouseListener extends MouseAdapter {
        private int selected = -1;
        
        public void mouseDragged(MouseEvent e) {
            selected = -1;
            if (!listScroller.getVerticalScrollBar().isEnabled()) {
                listScroller.getVerticalScrollBar().setEnabled(true);
            }
        }
        public void mouseMoved(MouseEvent e) {
            selected = -1;
        }
        public void mousePressed(MouseEvent ev)  {
            selected = -1;
        }
        public void mouseReleased(MouseEvent ev) {
            if (selected != -1) {
                contentList.setSelectedIndex(selected);
            } else {
                contentList.setSelectedIndex(-1);
            }
            listScroller.getVerticalScrollBar().setEnabled(false);
        }
        public void mouseClicked(MouseEvent ev)  {
            selected = contentList.getSelectedIndex();
            List<Object> ob = contentList.getSelectedValuesList();
            if (ob.size() > 1) return;
            if (removable && ev.getClickCount() == 2) {
                listModel.removeElement(ob.get(0));
            }
            if (al != null) {
                contentList.setSelectedIndex(contentList.locationToIndex(ev.getPoint()));
                al.actionPerformed(new ActionEvent(contentList,
                        ActionEvent.ACTION_PERFORMED,
                        ob.toString()));
                ev.consume();
            }
        }
    }
}
