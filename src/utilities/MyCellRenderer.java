package utilities;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

@SuppressWarnings("serial")
public class MyCellRenderer extends JPanel implements ListCellRenderer<Object> {

    JLabel lbl = new JLabel();  
    public MyCellRenderer() {
        setPreferredSize(new Dimension(100, 50));
        setLayout(new GridLayout());
        lbl = new JLabel("",JLabel.LEFT);
        lbl.setFont(new Font("Roboto", Font.PLAIN, 25));
        lbl.setForeground(Color.BLACK);
        add(lbl);
    }

    @Override
    public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list,Object value,  
            int index,boolean isSelected,boolean cellHasFocus)  {
        if (value != null) {
            lbl.setText("  " + value.toString()); 
        }
            
        if(isSelected && !list.getValueIsAdjusting()) {
            setBackground(Settings.instance().backgroundSelected);  
        }
        else {
            setBackground(Settings.instance().background);  
        }
        return this;
    }
}