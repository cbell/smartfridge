package utilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ToggleButton extends JPanel implements MouseListener {

	private Image deselectedImage;
	private Image selectedImage;

	private boolean selected;
	private boolean scaled;

	/**
	 * Create a new ToggleButton with a deselected image and a selected image.
	 * 
	 * @param deselectedImage
	 *            the image to display when the button is 'off'
	 * @param selectedImage
	 *            the image to display when the button is 'on'
	 */
	public ToggleButton(Image deselectedImage, Image selectedImage) {
		this.deselectedImage = deselectedImage;
		this.selectedImage = selectedImage;
		selected = false;
		scaled = false;

		setBackground(new Color(0, 0, 0, 0));
		
		addMouseListener(this);
	}

	public boolean isSelected() {
		return selected;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (!scaled) {
			selectedImage = selectedImage.getScaledInstance(-1, getParent()
					.getPreferredSize().height, Image.SCALE_SMOOTH);
			deselectedImage = deselectedImage.getScaledInstance(-1, getParent()
					.getPreferredSize().height, Image.SCALE_SMOOTH);
			while (selectedImage.getWidth(this) < 0
					|| deselectedImage.getWidth(this) < 0)
				;
			setPreferredSize(new Dimension(selectedImage.getWidth(this),
					selectedImage.getHeight(this)));
			scaled = true;
		}

		Image scaled;
		if (selected) {
			scaled = selectedImage;
		} else {
			scaled = deselectedImage;
		}

		g.drawImage(scaled, 0, 0, new Color(0, 0, 0, 0), this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		selected = !selected;
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}
}
