package utilities;

import java.awt.Color;

public class Settings {

    static private Settings _instance = null;
    // List of properties
    public Color background = null;
    public Color backgroundSelected = null;
    
    
    private Settings() {
            
            String backgroundRed =   "0.29";
            String backgroundGreen = "0.78";
            String backgroundBlue =  "0.98";
            String backgroundAlpha = "1.00";
            background = new Color(Float.parseFloat(backgroundRed), 
                                   Float.parseFloat(backgroundGreen),
                                   Float.parseFloat(backgroundBlue), 
                                   Float.parseFloat(backgroundAlpha));
            int r = background.getRed()   + 20 < 256 ? background.getRed()   + 20 : 255;
            int g = background.getGreen() + 20 < 256 ? background.getGreen() + 20 : 255;
            int b = background.getBlue()  + 20 < 256 ? background.getBlue()  + 20 : 255;
            int a = background.getAlpha() + 20 < 256 ? background.getAlpha() + 20 : 255;
            backgroundSelected = new Color(r, g, b, a);   
    }
    
    static public Settings instance(){
        if (_instance == null) {
            _instance = new Settings();
        }
        return _instance;
    }
}
