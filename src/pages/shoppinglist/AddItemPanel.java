package pages.shoppinglist;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataBase.ContentsObject;
import dataBase.ContentsObject.Contents;


import utilities.MyScrollingList;
import utilities.Settings;

@SuppressWarnings("serial")
public class AddItemPanel extends JPanel {
    private ActionListener al;
    private Font bigFont;
    private JTextField searchFeild;
    private JButton searchBtn;
    private MyScrollingList foundItems;

    public AddItemPanel(ActionListener al) {
        super();
        this.al = al;
        initialize();
    }
    
    private void initialize() {
        setBackground(Settings.instance().background);
        
        GridBagLayout gbl1 = new GridBagLayout();
        gbl1.columnWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0};
        gbl1.columnWidths = new int[] {20, 0, 20, 0, 20};
        gbl1.rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0};
        gbl1.rowHeights = new int[] {20, 0, 0, 0, 20};
        setLayout(gbl1);
        GridBagConstraints c = new GridBagConstraints();
        
        bigFont = new Font("Roboto", Font.PLAIN, 25);
        JLabel newItem = new JLabel("New Item:");
        newItem.setFont(bigFont);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        c.gridheight = 1;
        add(newItem, c);
        
        searchFeild = new JTextField();
        searchFeild.setFont(bigFont);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 1;
        c.gridheight = 1;
        add(searchFeild, c);
        
        
        searchBtn = new JButton("Search");
        searchBtn.addActionListener(new SearchBtnListener());
        searchBtn.setFont(bigFont);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 3;
        c.gridy = 2;
        c.gridwidth = 1;
        c.gridheight = 1;
        add(searchBtn, c);
        
        foundItems = new MyScrollingList(new String[] {});
        foundItems.setBackground(Settings.instance().background);
        foundItems.setActionListener(al);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth = 3;
        c.gridheight = 1;
        add(foundItems, c);
    }
    
    private class SearchBtnListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            foundItems.removeAll();
            try {
                String text = searchFeild.getText().toUpperCase();
                text = text.replace(' ', '_');
                Contents item = Contents.valueOf(text);
                Object[] obj = new Object[] {new ContentsObject(item)};
                foundItems.addItems(obj);
            } catch (Exception e2) {
                // TODO: handle exception
            }
        }
    }
}
