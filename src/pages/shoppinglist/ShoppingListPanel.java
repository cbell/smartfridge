package pages.shoppinglist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;

import dataBase.ContentsObject;
import dataBase.ContentsObject.Contents;

import utilities.MyScrollingList;
import utilities.Settings;

@SuppressWarnings("serial")
public class ShoppingListPanel extends JPanel {
    private ActionListener al;
    private MyScrollingList listScroller = null;
    public ShoppingListPanel() {
        super();
        initialize();
    }
    
    private void initialize() {
        setLayout(new BorderLayout());
        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        
        al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof ContentsObject) {
                    Object[] obj = new Object[] {((ContentsObject)e.getSource())};
                    listScroller.addItems(obj);
                } else {
                    @SuppressWarnings("unchecked")
                    Object[] obj = new Object[] {((ContentsObject) ((JList<Object>)e.getSource()).getSelectedValue())};
                    listScroller.addItems(obj);
                }
            }
        };
        
        ContentsObject[] shoppingList = new ContentsObject[] {
                new ContentsObject(Contents.BEER),
                new ContentsObject(Contents.EGGS),
                new ContentsObject(Contents.TOMATOES)};

        listScroller = new MyScrollingList(shoppingList);
        listScroller.setRemovable(true);
        
        add(listScroller, BorderLayout.CENTER);
        
        JPanel pane = new JPanel();
        pane.setBackground(Settings.instance().background);
        pane.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 0, Color.BLACK));
        JButton button;
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c2 = new GridBagConstraints();
        button = new JButton("Share");
        button.setFont(new Font("Roboto", Font.PLAIN, 17));
        c2.weightx = 0.5;
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.ipady = 25;      //make this component tall
        c2.gridx = 0;
        c2.gridy = 0;
        c2.insets = new Insets(5,5,0,0);
        pane.add(button, c2);

        button = new JButton("Remove");
        button.setFont(new Font("Roboto", Font.PLAIN, 17));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listScroller.removeSelected();
            }
        });
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.ipady = 25;      //make this component tall
        c2.weightx = 0.5;
        c2.gridx = 1;
        c2.gridy = 0;
        c2.insets = new Insets(5,5,0,5);
        pane.add(button, c2);

        button = new JButton("Proceed to Checkout");
        button.setFont(new Font("Roboto", Font.PLAIN, 17));
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.ipady = 25;      //make this component tall
        c2.weightx = 0.0;
        c2.gridwidth = 2;
        c2.gridx = 0;
        c2.gridy = 1;
        c2.insets = new Insets(5, 5, 5, 5);
        pane.add(button, c2);
        add(pane, BorderLayout.SOUTH);
        
    }
    
    public ActionListener getActionListener() {
        return al;
    }
}
