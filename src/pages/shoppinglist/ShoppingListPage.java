package pages.shoppinglist;

import java.awt.event.ActionListener;

import pages.PageTemp2Windows;

@SuppressWarnings("serial")
public class ShoppingListPage extends PageTemp2Windows {
    ShoppingListPanel slp;
    
    public ShoppingListPage() {
        super();
        initialize();
    }
    
    private void initialize() {
        slp = new ShoppingListPanel();
        setWindow1Contents(slp);
        setWindow2Contents(new AddItemPanel(slp.getActionListener()));
    }
    
    public ActionListener getActionListener() {
        return slp.getActionListener();
    }
}
