package pages.settings;

import pages.PageTemp2Windows;

@SuppressWarnings("serial")
public class SettingsPage extends PageTemp2Windows {

    public SettingsPage() {
        super();
        initialize();
    }
    
    private void initialize() {
        setWindow1Contents(new SettingsListPanel());
        setWindow2Contents(new SettingsPanel());
    }
}
