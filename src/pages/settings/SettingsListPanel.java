package pages.settings;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import utilities.MyScrollingList;

@SuppressWarnings("serial")
public class SettingsListPanel extends JPanel{
    private MyScrollingList listScroller = null;
    
    public SettingsListPanel() {
        super();
        initialize();
    }
    
    private void initialize() {
        setLayout(new BorderLayout());
        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        
        String[] settignsList = new String[] {"Wi-Fi", "AmazonAcount", 
                "Calendar Account", "Amazon Fresh Settings", "Wallpaper", 
                "Fridge Temperature", "Weather Location"};

        listScroller = new MyScrollingList(settignsList);
        
        add(listScroller, BorderLayout.CENTER);
    }
}
