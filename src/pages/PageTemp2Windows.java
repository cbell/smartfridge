package pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import utilities.Settings;

@SuppressWarnings("serial")
public class PageTemp2Windows extends JPanel {
    private JPanel window1 = null;
    private JPanel window2 = null;
    
    public PageTemp2Windows() {
        super();
        initialize();
    }
    
    private void initialize() {
        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        
        GridBagLayout gbl = new GridBagLayout();
        gbl.columnWeights = new double[] {0.0, 1.0, 0.0, 1.0, 0.0};
        gbl.columnWidths = new int[] {10, 0, 10, 0, 10};
        gbl.rowWeights = new double[] {0.0, 1.0, 0.0};
        gbl.rowHeights = new int[] {10, 0, 10};
        setLayout(gbl);
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        
        window1 = new JPanel();
        window1.setLayout(new GridLayout());
        window1.setPreferredSize(new Dimension(400, 400));
        window1.setBackground(Settings.instance().background);
        window1.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(window1, gbc);
        
        window2 = new JPanel();
        window2.setLayout(new GridLayout());
        window2.setPreferredSize(new Dimension(400, 400));
        window2.setBackground(Settings.instance().background);
        window2.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
        gbc.gridx = 3;
        gbc.gridy = 1;
        add(window2, gbc);
    }
    
    public void setWindow1Contents(JPanel contents) {
        window1.removeAll();
        if (contents != null) {
            window1.add(contents);
        }
    }
    
    public void setWindow2Contents(JPanel contents) {
        if (contents != null) {
            window2.add(contents);
        }
    }
}
