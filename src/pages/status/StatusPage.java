package pages.status;

import pages.PageTemp1Window;

@SuppressWarnings("serial")
public class StatusPage extends PageTemp1Window {

    public StatusPage() {
        super();
        initialize();
    }
    
    private void initialize() {
        setWindow1Contents(new StatusPanel());
    }
}
