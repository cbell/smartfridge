package pages.status;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import utilities.Settings;

@SuppressWarnings("serial")
public class StatusPanel extends JPanel {
    
    public StatusPanel() {
        super();
        initialize();
    }
    
    private void initialize() {
        setLayout(new GridLayout(2, 0));
        setBackground(Settings.instance().background);
        JLabel label1 = new JLabel("The fridge status will show up here");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setFont(new Font("Roboto", Font.PLAIN, 30));
        add(label1);
        JLabel label2 = new JLabel("(not yet implemented)");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setFont(new Font("Roboto", Font.PLAIN, 30));
        add(label2);
    }
}
