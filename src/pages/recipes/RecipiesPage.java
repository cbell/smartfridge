package pages.recipes;

import pages.PageTemp2Windows;

@SuppressWarnings("serial")
public class RecipiesPage extends PageTemp2Windows{

    public RecipiesPage() {
        super();
        initialize();
    }
    
    private void initialize() {
        setWindow1Contents(new RecipieListPanel());
        setWindow2Contents(new RecipiePanel());
    }
}
