package pages.recipes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JPanel;

import utilities.MyScrollingList;

@SuppressWarnings("serial")
public class RecipieListPanel extends JPanel {
    private MyScrollingList categoryScroller = null;
    private MyScrollingList recipeScroller = null;

    public RecipieListPanel() {
        super();
        initialize();
    }
    
    private void initialize() {
        setLayout(new GridLayout(1, 2));
        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        
        String[] categories = new String[] {"Favorites", "Appetizers", 
                "Beverages", "Bread", "Breakfast", "Desserts", 
                "Meats", "Pasta", "Rice", "Salads", "Sandwiches", "Sauces", 
                "Seafood", "Soups"};
        
        final String[] favorites = new String[] {"Death By Chocolate", "Fettuccine Alfredo", "Seven Layer Dip", "Scrambled Eggs", "Lemon Chicken", "Fried Rice", "Caesar Salad", "Lemon Butter Sauce", "Grilled Salmon", "Pumpkin Bread", "Pork Loin", "White Russian", "Turkey Sandwich", "Minestrone Soup"};
        final String[] appetizers = new String[] {"Pita Bread", "Cheese Platter", "Spinach Dip", "Seven Layer Dip"};
        final String[] beverages = new String[] {"Fruit Punch", "Martini", "White Russian", "Cosmopolitan", "Margarita"};
        final String[] bread = new String[] {"Pumpkin Bread", "Banana Bread", "Apple Bread", "Dinner Rolls"};
        final String[] breakfast = new String[] {"Scrambled Eggs", "Pancakes", "Banana Pancakes", "Waffles", "Chocolate Chip Pancakes"};
        final String[] desserts = new String[] { "Chocoloate Cake", "Death By Chocolate", "Red Velvet Cake", "Carrot Cake", "Lemon Squares", "Pumpkin Bread", "Banana Bread"};
        final String[] meats = new String[] {"Lemon Chicken", "Simple Steak", "Teriyaki Chicken", "Pork Loin"};
        final String[] pasta = new String[] {"Fettuccine Alfredo", "Spaghetti", "Eggplant Linguini", "Stuffed Shells"};
        final String[] rice = new String[] { "Fried Rice", "Chicken Fried Rice", "Rice Pudding", "Spanish Rice"};
        final String[] salads = new String[] { "Mixed Greens", "Caesar", "Harvest", "Spinach"};
        final String[] sandwiches = new String[] {"BLT", "Club", "Turkey", "Chicken Parmesan"};
        final String[] sauces = new String[] {"Alfredo", "Marinara", "Meat Sauce", "Lemon Butter", "Red Wine Reduction"};
        final String[] seafood = new String[] {"Grilled Salmon", "Tilapia and Rice", "Crab Cakes"};
        final String[] soups = new String[] {"Chicken Tomato Soup", "Clam Chowder", "Minestrone", "Potato Soup"};
        
        categoryScroller = new MyScrollingList(categories);
        add(categoryScroller, BorderLayout.CENTER);
        
        recipeScroller = new MyScrollingList(new String[] {});
        add(recipeScroller, BorderLayout.CENTER);
        
        categoryScroller.setActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                recipeScroller.removeAll();
                @SuppressWarnings("unchecked")
                String cat = (String) ((JList<Object>)arg0.getSource()).getSelectedValue();
                switch (cat) {
                case "Favorites":
                    recipeScroller.addItems(favorites);
                    break;
                case "Appetizers":
                    recipeScroller.addItems(appetizers);
                    break;
                case "Beverages":
                    recipeScroller.addItems(beverages);
                    break;
                case "Bread":
                    recipeScroller.addItems(bread);
                    break;
                case "Breakfast":
                    recipeScroller.addItems(breakfast);
                    break;
                case "Desserts":
                    recipeScroller.addItems(desserts);
                    break;
                case "Meats":
                    recipeScroller.addItems(meats);
                    break;
                case "Pasta":
                    recipeScroller.addItems(pasta);
                    break;
                case "Rice":
                    recipeScroller.addItems(rice);
                    break;
                case "Salads":
                    recipeScroller.addItems(salads);
                    break;
                case "Sandwiches":
                    recipeScroller.addItems(sandwiches);
                    break;
                case "Sauces":
                    recipeScroller.addItems(sauces);
                    break;
                case "Seafood":
                    recipeScroller.addItems(seafood);
                    break;
                case "Soups":
                    recipeScroller.addItems(soups);
                    break;

                default:
                    break;
                }
            }
        });
    }
    
    
}
