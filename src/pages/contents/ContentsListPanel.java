package pages.contents;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import dataBase.ContentsObject;
import dataBase.ContentsObject.Contents;

import utilities.MyScrollingList;
import utilities.Settings;

@SuppressWarnings("serial")
public class ContentsListPanel extends JPanel {
    private MyScrollingList listScroller = null;
    private ActionListener al;
    
    public ContentsListPanel(ActionListener a) {
        super();
        al = a;
        initialize();
    }
    
    private void initialize() {
        setLayout(new BorderLayout());
        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        
        ContentsObject[] currentContent = new ContentsObject[] {
                new ContentsObject(Contents.AMERICAN_CHEESE), 
                new ContentsObject(Contents.BACON),
                new ContentsObject(Contents.BEER),
                new ContentsObject(Contents.BUTTER),
                new ContentsObject(Contents.GROUND_BEEF),
                new ContentsObject(Contents.KETCHUP),
                new ContentsObject(Contents.MAYONNAISE),
                new ContentsObject(Contents.MILK),
                new ContentsObject(Contents.SALSA),
                new ContentsObject(Contents.STEAK)};

        listScroller = new MyScrollingList(currentContent);
        listScroller.setActionListener(al);
        
        add(listScroller, BorderLayout.CENTER);
        
        JButton sortBy = new JButton("Sort By");
        sortBy.setFont(new Font("Roboto", Font.PLAIN, 25));
        
        JPanel pane = new JPanel();
        pane.setBackground(Settings.instance().background);
        pane.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 0, Color.BLACK));
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c2 = new GridBagConstraints();
        c2.weightx = 0.5;
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.ipady = 25;      //make this component tall
        c2.gridx = 0;
        c2.gridy = 0;
        c2.insets = new Insets(5,5,5,5);
        pane.add(sortBy, c2);

        add(pane, BorderLayout.SOUTH);
    }
}
