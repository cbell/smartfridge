package pages.contents;

import java.awt.event.ActionListener;

import main.MainPanel;

import pages.PageTemp2Windows;

@SuppressWarnings("serial")
public class ContentsPage extends PageTemp2Windows {
    
    public ContentsPage(MainPanel mainPanel, ActionListener actionListener) {
        super();
        initialize(mainPanel, actionListener);
    }

    private void initialize(MainPanel mainPanel, ActionListener al) {
        ItemPanel ip = new ItemPanel(mainPanel, al);
        setWindow1Contents(new ContentsListPanel(ip.getActionListener()));
        setWindow2Contents(ip);
    }
}
