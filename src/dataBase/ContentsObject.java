package dataBase;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ContentsObject {
    private String name;
    private String brand;
    private String purchased;
    private String expiration;
    private String amountRemaining;
    private String ingredients;
    private BufferedImage image;
    public enum Contents {AMERICAN_CHEESE, BACON, BEER, BUTTER, EGGS, 
        GROUND_BEEF, KETCHUP, MAYONNAISE, MILK, SALSA, STEAK, TOMATOES};
    
    public ContentsObject(Contents item) {
        setItem(item);
    }

    private void setItem(Contents item) {
        switch (item) {
        case AMERICAN_CHEESE:
            name = "American Cheese";
            brand = "Kraft";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "80%";
            ingredients = "MILK, WHEY, MILK PROTEIN CONCENTRATE, " + 
                    "MILKFAT, SODIUM CITRATE, CONTAINS LESS THAN 2% OF CALCIUM " +
                    "PHOSPHATE, WHEY PROTEIN CONCENTRATE, SALT, LACTIC ACID, " +
                    "SORBIC ACID AS A PRESERVATIVE, CHEESE CULTURE, ANNATTO AND " +
                    "PAPRIKA EXTRACT (COLOR), ENZYMES, VITAMIN D3. CONTAINS: MILK.";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents/american_cheese.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case BACON:
            name = "Bacon";
            brand = "John Morrell";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "50%";
            ingredients = "Bacon Cured With Water, Salt, Sugar, Smoke " + 
                          "Flavoring, Sodium Phosphate, Sodium Erythorbate, " +
                          "Sodium Nitrite.";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents/bacon.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case BEER:
            name = "Beer";
            brand = "Budweiser";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "2 Bottles";
            ingredients = "Barley malt, rice, hops, yeast, water.";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents/beer.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case BUTTER:
            name = "Butter";
            brand = "Land O'Lakes";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "Sweet Cream, Salt. CONTAINS: MILK";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents/butter.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case EGGS:
            name = "Eggs";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "Eggs";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case GROUND_BEEF:
            name = "Ground Beef";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case KETCHUP:
            name = "Ketchup";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
            
        case MAYONNAISE:
            name = "Mayonnaise";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
        case MILK:
            name = "Milk";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
        case SALSA:
            name = "Salsa";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
        case STEAK:
            name = "Steak";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
        case TOMATOES:
            name = "Tomatoes";
            brand = "";
            purchased = "11/29/12";
            expiration = "12/29/12";
            amountRemaining = "90%";
            ingredients = "";
            try {                
                image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("Icons/Contents.gif"));
            } catch (IOException ex) {
                // handle exception...
            }
            break;
        default:
            break;
        }
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getPurchased() {
        return purchased;
    }

    public String getExpiration() {
        return expiration;
    }

    public String getAmountRemaining() {
        return amountRemaining;
    }

    public String getIngredients() {
        return ingredients;
    }

    public BufferedImage getImage() {
        return image;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
