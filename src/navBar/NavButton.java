package navBar;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import utilities.Settings;


@SuppressWarnings("serial")
public class NavButton extends JPanel {
    private JLabel label = null;
    private Boolean selected = false;

    public NavButton(String text, ImageIcon icon) {
        super();
        initialize(text, icon);
    }

    /**
     * @param text
     * @param icon
     */
    private void initialize(String text, ImageIcon icon) {
        setLayout(new GridLayout());
        setPreferredSize(new Dimension(100, 700));
        if (text.length() != 0 && icon != null) {
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(60, 60,  java.awt.Image.SCALE_SMOOTH);  
            Icon newIcon = new ImageIcon(newimg);
            label = new JLabel(text, newIcon, JLabel.CENTER);
            label.setVerticalTextPosition(JLabel.BOTTOM);
            label.setHorizontalTextPosition(JLabel.CENTER);
            label.setFont(new Font("Roboto", Font.BOLD, 12));
        }
        setBackground(Settings.instance().background);
        add(label);
    }

    /**
     * @return true if selected, else false.
     */
    public Boolean isSelected() {
        return selected;
    }

    /**
     * @param selected The selected value to set.
     */
    public void setSelected(Boolean selected) {
        this.selected = selected;
        Color bg = Settings.instance().background;
        if (selected == true) {
            setBackground(Settings.instance().backgroundSelected);
        }
        else {
            setBackground(bg);
        }
    }
}
