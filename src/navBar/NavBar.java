package navBar;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import main.MainPanel;
import utilities.Settings;


@SuppressWarnings("serial")
public class NavBar extends JPanel implements MouseListener {
    private MainPanel root;
    private NavButton contents = null;
    private NavButton shoppingList = null;
    private NavButton recipes = null;
    private NavButton status = null;
    private NavButton settings = null;
    private NavButton selected = null;
    
    public NavBar(MainPanel root) {
        super();
        this.root = root;
        initialize();
    }
    
    private void initialize() {
        setLayout(new GridLayout(5, 1));
        setBackground(Settings.instance().background);
        
        ImageIcon icon1 = new ImageIcon(getClass().getClassLoader().getResource("Icons/Contents.gif"));
        contents = new NavButton("Contents", icon1);
        contents.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.BLACK));
        contents.addMouseListener(this);
        add(contents);
        
        ImageIcon icon2 = new ImageIcon(getClass().getClassLoader().getResource("Icons/Shopping_List.gif"));
        shoppingList = new NavButton("Shopping List", icon2);
        shoppingList.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.BLACK));
        shoppingList.addMouseListener(this);
        add(shoppingList);
        
        ImageIcon icon3 = new ImageIcon(getClass().getClassLoader().getResource("Icons/Recipes.gif"));
        recipes = new NavButton("Recipes", icon3);
        recipes.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.BLACK));
        recipes.addMouseListener(this);
        add(recipes);
        
        ImageIcon icon4 = new ImageIcon(getClass().getClassLoader().getResource("Icons/Status.gif"));
        status = new NavButton("Status", icon4);
        status.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.BLACK));
        status.addMouseListener(this);
        add(status);
        
        ImageIcon icon5 = new ImageIcon(getClass().getClassLoader().getResource("Icons/Settings.gif"));
        settings = new NavButton("Settings", icon5);
        settings.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.BLACK));
        settings.addMouseListener(this);
        add(settings);
    }
    
    public void setSelected(String page) {
        selected = null;
        contents.setSelected(false);
        shoppingList.setSelected(false);
        recipes.setSelected(false);
        status.setSelected(false);
        settings.setSelected(false);

        switch (page) {
            case MainPanel.CONTENTPAGE:
                contents.setSelected(true);
                selected = contents;
                break;
            case MainPanel.SHOPPINGLISTPAGE:
                shoppingList.setSelected(true);
                selected = shoppingList;
                break;
            case MainPanel.RECIPESPAGE:
                recipes.setSelected(true);
                selected = recipes;
                break;
            case MainPanel.STATUSPAGE:
                status.setSelected(true);
                selected = status;
                break;
            case MainPanel.SETTINGSPAGE:
                settings.setSelected(true);
                selected = settings;
                break;
        default:
            break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Object source = e.getSource();
        if (source != selected) {
            contents.setSelected(false);
            shoppingList.setSelected(false);
            recipes.setSelected(false);
            status.setSelected(false);
            settings.setSelected(false);
            
            if (source == contents) {
                contents.setSelected(true);
                selected = contents;
                root.setPage(MainPanel.CONTENTPAGE);
            }
            else if (source == shoppingList) {
                shoppingList.setSelected(true);
                selected = shoppingList;
                root.setPage(MainPanel.SHOPPINGLISTPAGE);
            }
            else if (source == recipes) {
                recipes.setSelected(true);
                selected = recipes;
                root.setPage(MainPanel.RECIPESPAGE);
            }
            else if (source == status) {
                status.setSelected(true);
                selected = status;
                root.setPage(MainPanel.STATUSPAGE);
            }
            else if (source == settings) {
                settings.setSelected(true);
                selected = settings;
                root.setPage(MainPanel.SETTINGSPAGE);
            }
            root.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }

    @Override
    public void mousePressed(MouseEvent e) { }

    @Override
    public void mouseReleased(MouseEvent e) { }

}
